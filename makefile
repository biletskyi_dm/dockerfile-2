build:
	docker build -t hello-go:v2 .
size:
	docker image inspect hello-go:v2 --format='{{.Size}}' \
        | numfmt --to=iec
run:
	docker run --name hello-go -d -it -p 1212:80 hello-go:v2