FROM golang:alpine AS builder

WORKDIR /app

COPY hello.go hello.go

RUN CGO_ENABLED=0 \
    GOOS=linux \
    GOARCH=amd64 \
    go build -a -tags netgo -ldflags '-w' -o /app/hello

FROM scratch

COPY --from=builder /app/hello /app/hello

ENTRYPOINT ["/app/hello"]